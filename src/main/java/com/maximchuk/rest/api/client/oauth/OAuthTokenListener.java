package com.maximchuk.rest.api.client.oauth;

/**
 * @author Maxim L. Maximchuk
 *         Date: 11/21/13
 */
public interface OAuthTokenListener {

    void authorize(OAuthTokenEvent event);
    void refreshToken(OAuthTokenEvent event);

}
