package com.maximchuk.rest.api.client.oauth;

import com.maximchuk.rest.api.client.core.DefaultClient;
import com.maximchuk.rest.api.client.core.RestApiMethod;
import com.maximchuk.rest.api.client.core.RestApiResponse;
import com.maximchuk.rest.api.client.http.HttpException;

import java.io.IOException;

/**
 * Created by Maxim Maximchuk
 * date 08-Dec-15.
 */
public class OAuthClient extends DefaultClient {

    private OAuthCredentials credentials;

    public OAuthClient(String serverUrl, String controllerName, OAuthCredentials credentials) throws IOException, HttpException {
        super(serverUrl, controllerName);
        this.credentials = credentials;
        setCredentials(credentials);
        try {
            credentials.authorize();
        } catch (HttpException e) {
            if (e.getErrorCode() == 401) {
                credentials.refresh();
            }
        }
    }

    @Override
    public RestApiResponse execute(RestApiMethod method) throws IOException, HttpException {
        RestApiResponse response;
        try {
            response = super.execute(method);
        } catch (HttpException e) {
            if (e.getErrorCode() == 401) {
                credentials.refresh();
                response = super.execute(method);
            } else {
                throw e;
            }
        }
        return response;
    }
}
